import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('c409_printer_app', () => App);
