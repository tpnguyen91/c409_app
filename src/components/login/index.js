import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  DeviceEventEmitter,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  TextInput,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import {connect} from 'react-redux'
import {loginSuccess} from '../../actions';
import Spinner from 'react-native-loading-spinner-overlay';
import { login } from '../../common/services'
import FitImage from 'react-native-fit-image';

export const height = Dimensions.get('window').height;
export const width = Dimensions.get('window').width;

const versionNumber = '1.0.1' // 2/1/2018
const bg = require('../../images/bgApp.png');

class Login extends Component {

  static navigationOptions = {
    header: null
  }

  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
      visible: false,
    }
  }

  login() {
    const { username, password } = this.state;
    if (username === '' || password === '') {
      alert('Vùi lòng điền đầy đủ thông tin!!!');
      return
    }
    this.showLoading();
    login(JSON.stringify({username, password}))
    .then(res => {
      if(res) {
        if(res['role'] !== 'admin' && res['role'] === 'seller') {
          this.props.loginSuccess(res);
        }else {
          alert('Tài khoản của bạn không được cấp quyền truy cập vào điểm bán lẻ...')
        }
      }
    })
    .catch(err => {
      alert(err)
      console.log(err);
    })
    .then(() => this.hideLoading()); 
  }

  renderUserField() {
    const { username } = this.state;
    return (
      <View
        style={{
          flexDirection: 'row',
          borderColor: '#EEEEEE',
          borderWidth: 1,
          borderRadius: 5,
          width: width - 40,
          marginLeft: 20,
          marginTop: 10,
          backgroundColor: '#F5F5F5'
        }}
      >
        <TextInput
          {...commonInputProps}
          placeholder={'Tên Đăng Nhập'}
          value={username}
          autoCapitalize={'none'}
          onChangeText={(text) => {
              this.setState({ username: text })
          }}
        />
      </View>
    );
  }

  showLoading() {
    this.setState({ visible: true })
  }

  hideLoading() {
    this.setState({ visible: false })
  }

  renderPassword() {
    const { password } = this.state;
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderColor: '#EEEEEE',
          borderWidth: 1,
          borderRadius: 5,
          width: width - 40,
          marginLeft: 20,
          marginTop: 10,
          backgroundColor: '#F5F5F5'
        }}
      >
        <TextInput
          {...commonInputProps}
          placeholder={'Mật khẩu'}
          value={password}
          secureTextEntry={true}
          onChangeText={(text) => {
              this.setState({ password: text })
          }}
        />
      </View>
    );
  }

  renderLogin() {
    return (
      <TouchableOpacity
        style={{
          borderRadius: 5,
          backgroundColor: '#03A9F4',
          justifyContent: 'center',
          alignItems: 'center',
          width: width - 40,
          height: 50,
          marginLeft: 20,
          marginTop: 10,
        }}
        onPress={() => this.login()}
      >
        <Text
          style={{
            color: 'white',
            fontSize: 16,
            fontWeight: 'bold',
          }}
        >
          Đăng Nhập
        </Text>
      </TouchableOpacity>
    );
  }

  renLoginForm() {
    return (
      <View style={{ flex: 1}}>
          {this.renderUserField()}
          {this.renderPassword()}
          {this.renderLogin()}
      </View>
    );
  }

  renderVersion() {
    return (
      <View style={{ width, height: 30, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ position: 'absolute', bottom: 0, color: 'black', fontSize: 12 }}>Phiên bản {versionNumber}</Text>
      </View>
    )
  }


  render() {
    const { visible } = this.state;
    return (
      <KeyboardAvoidingView behavior='height' style={styles.container}>
        <ScrollView>
          <Image
            source={bg}
            resizeMode={'contain'}
            style={styles.imgLogo}
          />
          {this.renLoginForm()}
          {this.renderVersion()}
          <Spinner visible={visible} textStyle={{color: '#FFF'}} />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const commonInputProps = {
  height: 40,
  marginTop: 10,
  flex: 1,
  fontSize: 18,
  underlineColorAndroid: 'transparent',
  placeholderTextColor: 'rgba(0,0,0,0.5)',
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  imgLogo: {
    width,
    height: width
  },
});

export default connect(state => ({logged: state.authentication.loggedIn, user: state.authentication.user}), {loginSuccess})(Login);