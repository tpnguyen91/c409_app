import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  DeviceEventEmitter,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  ScrollView
} from 'react-native';
import {
  formatCurrency
} from '../../common/services.js';
import {logout} from '../../actions';
import Spinner from 'react-native-loading-spinner-overlay';
import DateTimePicker from 'react-native-modal-datetime-picker';
import PopupDialog, {SlideAnimation, DialogTitle} from 'react-native-popup-dialog';
import {connect} from 'react-redux'
import { fetchLottery } from '../../common/services'

const slideAnimation = new SlideAnimation({slideFrom: 'bottom'});

export const height = Dimensions
  .get('window')
  .height;
export const width = Dimensions
  .get('window')
  .width;

const API_URL = 'http://128.199.151.182';

class ListTickets extends Component {

  static navigationOptions = {
    drawerLabel: 'Xem Danh Sách Vé Bán',
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      listLotteries: [],
      visible: false,
      currentDate: new Date(),
      currentValue: '',
      isDateTimePickerVisible: false,
      totalCount: 0,
      totalMoney: 0,
    }
    this.fetchListLotteries = this.fetchListLotteries.bind(this);
    this._handleDatePicked = this._handleDatePicked.bind(this);
    this._hideDateTimePicker = this._hideDateTimePicker.bind(this);
    this.onUpdateValue = this.onUpdateValue.bind(this);
  }

  componentWillMount() {
    this.fetchListLotteries();
  }

  showLoading() {
    this.setState({visible: true})
  }

  hideLoading() {
    this.setState({visible: false})
  }

  fetchListLotteries() {
    const { currentDate, currentValue } = this.state;
    this.showLoading();
    let params = `day=${currentDate.getDate()}-${currentDate.getMonth() + 1}-${currentDate.getFullYear()}&type=dien-tu`
    if (currentValue !== '')
    {
      params = params + `&value=${currentValue}`;
    }
    fetchLottery(params)
      .then(res => {
        this.setState({
          listLotteries: res
            ? res.sort((b, a) => a.dateCreated - b.dateCreated)
            : []
        }, () => {
          if (this.state.listLotteries.length > 0) {
            let total = 0;
            let totalMoney = 0;
            this.state.listLotteries.forEach(v => {
              total = total + v.count;
              totalMoney = totalMoney + (v.count * parseInt(v.value));
            }) 
              this.setState({ totalCount: total, totalMoney });
          }else {
            this.setState({ totalCount: 0, totalMoney: 0 });
          }
        })
        this.hideLoading();
      })
      .catch(err => {
        this.hideLoading();
        console.log(err);
      })
  }

  renderHeaderList() {
    return (
      <View
        style={{
        height: 40,
        width,
        flexDirection: 'row',
        borderBottomColor: '#E0E0E0',
        borderBottomWidth: 1
      }}>
        <View
          style={{
          height: 40,
          width: 60,
          justifyContent: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            paddingLeft: 10,
            fontWeight: 'bold'
          }}>Số DT</Text>
        </View>
        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>Đài DT</Text>
        </View>
        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>Ngày Xổ</Text>
        </View>
        <View
          style={{
          height: 40,
          width: 60,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>Số Lượng</Text>
        </View>
        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            fontWeight: 'bold'
          }}>Mệnh Giá</Text>
        </View>
      </View>
    )
  }

  renderItem(item, index) {
    return (
      <View
        key={index}
        style={{
        height: 40,
        width,
        flexDirection: 'row',
        borderBottomColor: '#E0E0E0',
        borderBottomWidth: 1
      }}>
        <View
          style={{
          height: 40,
          width: 60,
          justifyContent: 'center'
        }}>
          <Text
            style={{
            fontSize: 12,
            paddingLeft: 10
          }}>{item.number}</Text>
        </View>

        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text style={{
            fontSize: 12
          }}>{item.channel ? item.channel.city : ''}</Text>
        </View>
        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text style={{
            fontSize: 12
          }}>{item.day || ''}</Text>
        </View>
        <View
          style={{
          height: 40,
          width: 60,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text style={{
            fontSize: 12
          }}>{item.count || 0}</Text>
        </View>
        <View
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <Text style={{
            fontSize: 12
          }}>{formatCurrency(item.value) || ''}</Text>
        </View>
      </View>
    );
  }

  renderHeader() {
    return (
      <View
        style={{
        height: 50,
        width,
        flexDirection: 'row'
      }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('SaleTicket', { fetchListLotteries: this.fetchListLotteries.bind(this) })}
          style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: '#D50000'
        }}>
          <Text
            style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18
          }}>Bán Vé</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('CheckTicket')}
          style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: '#0091EA'
        }}>
          <Text
            style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18
          }}>Kiểm Tra Vé</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.props.logout()}
          style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: '#BDBDBD'
        }}>
          <Text
            style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18
          }}>Thoát</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderListTicket() {
    const {listLotteries} = this.state;
    return (
      <ScrollView style={{
        flex: 1
      }}>
        {listLotteries.map((item, index) => this.renderItem(item, index))
}
      </ScrollView>
    );
  }

  renderFilterOption() {
    const { currentDate, currentValue } = this.state;
    return (
      <View
        style={{ width, height: 50, flexDirection: 'row', marginTop: 10, marginBottom: 5 }}
      >
        <TouchableOpacity
          onPress={() => this.setState({ isDateTimePickerVisible: true })}
          style={{ height: 50, borderWidth: 1, borderColor: '#90A4AE', borderRadius: 5, width: (width - 15) / 2, marginLeft: 5 }}
        >
          <Text style={{ fontSize: 12, color: '#BDBDBD', fontWeight: 'bold', marginLeft: 5, marginTop: 5 }}>
            Ngày Bán
          </Text>
          <Text style={{ fontSize: 16, color: 'black', fontWeight: 'bold', marginLeft: 5 }}>
            {`${currentDate.getDate()} - ${currentDate.getMonth() + 1} - ${currentDate.getFullYear()}`}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.popupDialog.show()}
          style={{ height: 50, borderWidth: 1, borderColor: '#90A4AE', borderRadius: 5, width: (width - 15) / 2, marginLeft: 5 }}
        >
          <Text style={{ fontSize: 12, color: '#BDBDBD', fontWeight: 'bold', marginLeft: 5, marginTop: 5 }}>
            Mệnh giá
          </Text>
          <Text style={{ fontSize: 16, color: 'black', fontWeight: 'bold', marginLeft: 5 }}>
            {currentValue === "" ? 'Xem Tất Cả Mệnh Giá' : formatCurrency(currentValue)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  
  _handleDatePicked = (date) => {
    this.setState({ currentDate: date }, () => {
      this.fetchListLotteries();
    })
    this._hideDateTimePicker()
  };
  

  renderDate() {
    return (
      <DateTimePicker
        isVisible={this.state.isDateTimePickerVisible}
        onConfirm={this._handleDatePicked}
        onCancel={this._hideDateTimePicker}
      />
    )
  }

  onUpdateValue(value) {
    this.setState({ currentValue: value }, () => {
      this.fetchListLotteries();
    }) 
    this.popupDialog.dismiss()
  }

  renderFilterValue() {
    return (
      <PopupDialog
        ref={(popupDialog) => {
        this.popupDialog = popupDialog;
      }}
        dialogTitle={< DialogTitle title = "Lọc Mệnh Giá " />}>
        <View>
          <TouchableOpacity
            style={{ width, height: 50, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.onUpdateValue('')}
          >
            <Text
              style={{ fontSize: 18, fontWeight: '500' }}
            >Xem Tất Cả Mệnh Giá</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ width, height: 50, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.onUpdateValue('10000')}
          >
            <Text
              style={{ fontSize: 18, fontWeight: '500' }}
            >{formatCurrency('10000')}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ width, height: 50, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.onUpdateValue('20000')}
          >
            <Text
              style={{ fontSize: 18, fontWeight: '500' }}
            >{formatCurrency('20000')}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ width, height: 50, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.onUpdateValue('50000')}
          >
            <Text
              style={{ fontSize: 18, fontWeight: '500' }}
            >{formatCurrency('50000')}</Text>
          </TouchableOpacity>
        </View>
      </PopupDialog>
    )
  }

  renderTotal() {
    const { totalCount, currentValue, totalMoney } = this.state;
    return (
      <View style={{ height: 60, width, marginBottom: 0, backgroundColor: '#03A9F4', flexDirection: 'row' }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>TỔNG TIỀN: </Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>SL: {totalCount}</Text>
          <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'black' }}>{formatCurrency(totalMoney)} đ</Text>
        </View>
      </View>
    )
  }

  render() {
    const {visible} = this.state;
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderFilterOption()}
        {this.renderHeaderList()}
        {this.renderListTicket()}
        {this.renderTotal()}
        {this.renderDate()}
        {this.renderFilterValue()}
        <Spinner visible={visible} textStyle={{
          color: '#FFF'
        }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  icon: {
    width: 24,
    height: 24
  }
});

export default connect(state => ({logged: state.authentication.loggedIn, user: state.authentication.user}), {logout})(ListTickets);