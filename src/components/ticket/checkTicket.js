import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  DeviceEventEmitter,
  TouchableOpacity,
  Alert,
  Dimensions,
  Image,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import Camera from 'react-native-camera';
import {fetchLotteryById, formatCurrency} from '../../common/services.js';
export const height = Dimensions
  .get('window')
  .height;
export const width = Dimensions
  .get('window')
  .width;
import Spinner from 'react-native-loading-spinner-overlay';

import PopupDialog, {SlideAnimation, DialogTitle} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({slideFrom: 'bottom'});

export default class CheckTickets extends Component {

  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      qrText: 'q',
      rs: undefined,
      visible: false,
      isScan: true,
      code: ''
    }
    this.onFetchLotteryById = this
      .onFetchLotteryById
      .bind(this);
  }

  componentDidMount() {}

  showLoading() {
    this.setState({visible: true})
  }

  hideLoading() {
    this.setState({visible: false})
  }

  renderResult() {
    const {rs} = this.state;
    return (
      <View style={{
        flex: 1
      }}>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Đài dự thưởng:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.channel['city']}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Ngày dự thưởng:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.day}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Đài dự thưởng:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.channel['city']}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Số:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.number}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Loại Lô:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.prize['name']}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Mệnh giá:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{formatCurrency(rs.value)}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Số lượng:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.count}</Text>
        </View>
        <View style={{
          flexDirection: 'row',
          margin: 5
        }}>
          <Text
            style={{
            fontSize: 14,
            fontWeight: '400'
          }}>Chiết Khấu:
          </Text>
          <Text style={{
            fontSize: 14
          }}>{rs.discount || '0'}
            %</Text>
        </View>
      </View>
    )
  }

  renderHeader() {
    return (
      <View
        style={{
        width,
        height: 50,
        flexDirection: 'row'
      }}>
        <TouchableOpacity
          onPress={() => this.setState({ isScan: true })}
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#D50000'
        }}>
          <Text
            style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18
          }}>
            Quét Mã
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ isScan: false })}
          style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#0091EA'
        }}>
          <Text
            style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 18
          }}>
            Nhập Mã
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.goBack()
          }}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#BDBDBD'
          }}
        >
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>DS Vé Bán</Text>
        </TouchableOpacity>
      </View>
    )
  }

  onFetchLotteryById(id) {
    this.setState({qrText: id});
    this.showLoading();
    fetchLotteryById(id).then((res) => {
      this.setState({
        rs: res || undefined
      }, () => {
        this.popupDialog.show();
      })
    })
    .catch((err) => {
      this.setState({
        rs: undefined
      }, () => {
        this.popupDialog.show();
      })
    })
    .then(() => this.hideLoading());
  }

  renderCamera() {
    return (
      <View style={{
        flex: 1
      }}>
        <Camera
          ref={(cam) => {
          this.camera = cam;
        }}
          style={{
          flex: 1
        }}
          onBarCodeRead={(data, bounds) => this.onFetchLotteryById(data.data)}
          aspect={Camera.constants.Aspect.fill}></Camera>
      </View>
    )
  }

  renderInputCode() {
    const {code} = this.state;
    return (
      <View
        style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <View
          style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderColor: 'rgba(0,0,0,0.3)',
          borderBottomWidth: 1,
          width: width - 40,
          marginLeft: 10,
          marginBottom: 10
        }}>
          <TextInput
            {...commonInputProps}
            placeholder={'Nhập mã...'}
            value={code}
            onChangeText={(text) => {
            this.setState({code: text})
          }}/>
        </View>
        <TouchableOpacity
          onPress={() => this.onFetchLotteryById(code)}
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: 40,
            backgroundColor: '#FF5722',
            marginTop: 10,
            width: 200
          }}
        >
          <Text
            style={{ color: 'white', fontSize: 16 }}
          >Kiểm tra</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderNotFound() {
    const {rs, qrText} = this.state;
    return (
      <View
        style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Text style={{
          fontSize: 16,
          fontWeight: 'bold'
        }}>
          Vé Không Tồn Tại Thử Lại
        </Text>
      </View>
    )
  }

  renderDialog() {
    const {rs} = this.state;
    return (
      <PopupDialog
        ref={(popupDialog) => {
        this.popupDialog = popupDialog;
      }}
        dialogTitle={< DialogTitle title = "Kết quả kiểm tra" />}>
        {
          rs
          ? this.renderResult()
          : this.renderNotFound()
        }
      </PopupDialog>
    )
  }

  renderViewScan() {
    const {qrText, visible, rs, isScan} = this.state
    return (qrText === ""
      ? this.renderCamera()
      : <TouchableOpacity
        onPress={() => this.setState({qrText: '', rs: undefined})}
        style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4FC3F7',
      }}>
        <Text
          style={{
          color: 'white',
          fontSize: 25
        }}>
          Hiển thị màn hình quét mã
        </Text>
      </TouchableOpacity>)
  }

  render() {
    const {qrText, visible, rs, isScan} = this.state
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {isScan ? this.renderViewScan() : this.renderInputCode()}
        {this.renderDialog()}
        <Spinner visible={visible} textStyle={{ color: '#FFF' }}/>
      </View>
    );
  }
}

const commonInputProps = {
  height: 40,
  marginTop: 10,
  flex: 1,
  fontSize: 16,
  underlineColorAndroid: 'transparent',
  placeholderTextColor: 'rgba(0,0,0,0.5)'
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  icon: {
    width: 24,
    height: 24
  }
});
