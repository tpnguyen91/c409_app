import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  DeviceEventEmitter,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {
  fetchMe, fetchChannel, fetchTypeLotteries, formatCurrency, fetchPrizeStructures,
  submitLottery, fetchStatistics
} from '../../common/services.js';
import SunmiInnerPrinter from 'react-native-sunmi-inner-printer';
import {connect} from 'react-redux';
import Modal from 'react-native-modalbox';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import Spinner from 'react-native-loading-spinner-overlay';
import DateTimePicker from 'react-native-modal-datetime-picker';

export const height = Dimensions.get('window').height;
export const width = Dimensions.get('window').width;

console.disableYellowBox = true;

const readNumber = {
  0: 'Không',
  1: 'Một',
  2: 'Hai',
  3: 'Ba',
  4: 'Bốn',
  5: 'Năm',
  6: 'Sáu',
  7: 'Bảy',
  8: 'Tám',
  9: 'Chín',
}

class SaleTickets extends Component {

  static navigationOptions = {    
    header: null
  }  

  constructor(props){
    super(props);
    this.callAPI();
    this.state = {
      isReady: true,
      saleDate: new Date(),
      channels: [],
      cities: [],
      valueLotteries: [],
      prizes: [],
      prizeStructures: [],
      choicedChannel: {},
      choicedCity: {},
      choicedValue: '',
      choicedStructure: {},
      choicedPrize: {},
      number: '',
      countItems: '1',
      CK: '',
      errors: {},
      isShowModal: false,
      isShowModalTypeNumber: false,
      isShowModalPrize: false,
      isShowModalValueLottery: false,
      visible: true,
      isDateTimePickerVisible: false,
      printer_status: ''
    };
    this.showLoading = this.showLoading.bind(this)    
    this.hideLoading = this.hideLoading.bind(this)
    this.renderHeaderModal = this.renderHeaderModal.bind(this)
    this.insert = this.insert.bind(this)
    
  }

  componentWillMount () {
    try {
      this._printerStatusListener = DeviceEventEmitter.addListener('PrinterStatus', action => {
          switch (action) {
              case SunmiInnerPrinter.Constants.NORMAL_ACTION:   // 可以打印
                  // your code
                  this.setState({
                      printer_status:"printer normal"
                  });
                  break;
              case SunmiInnerPrinter.Constants.OUT_OF_PAPER_ACTION:  // 缺纸异常
                  // your code
                  this.setState({
                      printer_status:"printer out out page"
                  });
                  break;
              case SunmiInnerPrinter.Constants.COVER_OPEN_ACTION:   // 开盖子
                  // your code
                  this.setState({
                      printer_status:"printer cover open"
                  });
                  break;
              default:
              // your code
                  this.setState({
                      printer_status:"printer status:"+action
                  });
          }
      });
  }catch(e){
      this.setState({
          printer_status:"printer error:"+e.message
      });
  };

  }

  componentWillUnmount() {
    this._printerStatusListener.remove();
  }
  
  showLoading() {
    this.setState({visible: true})
  }

  hideLoading() {
    this.setState({visible: false})
  }

  changeNumber( value ) {
    const { choicedStructure, errors } = this.state;

    if (!choicedStructure.length) return;

    const maxLength = parseInt(choicedStructure.length);

    let newValue = value.substring(0, maxLength);

    errors.number = '';

    this.setState({ number: newValue, errors });
  }

  choiceValueLottery(value) {
    const { errors } = this.state;
    errors['value'] = '';

    this.setState({ choicedValue: value, errors, isShowModalValueLottery: false });
  }

  choiceStructure(value) {
    const self = this;
    const { prizeStructures, number, errors } = this.state;

    const choicedStructure = prizeStructures.filter(p => p.length === value)[0] || {};
    const { prizes = [] } = choicedStructure;

    errors.structure = '';
    console.log('prizes', prizes);

    this.setState({ choicedStructure, prizes, errors, isShowModalTypeNumber: false , choicedPrize: {} }, () => {
      self.changeNumber(number);
    });
  }

  choicePrize(value) {
    const { prizes, errors } = this.state;

    const choicedPrize = prizes.filter(p => p.code === value)[0] || {};

    errors.prize = '';
    
    this.setState({ choicedPrize, isShowModalPrize: false });
  }

  callAPI() {
    const funcs = [this.fetchChannels.bind(this), this.fetchValueLotteries.bind(this)];
    this.showLoading()
    Promise.all(funcs.map(f => f()))
      .then(() => {
        this.setState({ isReady: true })
        this.hideLoading()
      })
      .catch((err) => {
        this.setState({ isReady: false })
        this.hideLoading()
      })
  }

  checkValidate() {
    const errors = {};
    const {
      choicedValue, choicedCity, choicedChannel, choicedStructure,
      choicedPrize, saleDate, number
    } = this.state;

    if (!choicedValue) {
      errors['value'] = 'Chưa chọn mệnh giá';
    }
    if (!choicedChannel._id) {
      errors['channel'] = 'Chưa chọn nhà phát hành';
    }
    if (!choicedStructure._id) {
      errors['structure'] = 'Chưa chọn loại số';
    }
    if (!choicedPrize._id) {
      errors['prize'] = `Chưa chọn lôr`;
    }
    if (!saleDate) {
      errors['date'] = 'Chưa chọn ngày';
    }
    if (!number) {
      errors['number'] = 'Chưa chọn số';
    }
    else if (choicedStructure._id && number.length < choicedStructure.length) {
      errors['number'] = 'Loại số nhập vào không đúng';
    }
    return errors;
  }

  resetField() {
    this.setState({ number: '', CK: '0', choicedPrize: {}, countItems: '1' })
  }

  insert(isPrint) {
    const errors = this.checkValidate();
    
    if (Object.keys(errors).length) {
      return alert(Object.keys(errors).map(v => errors[v]).join(', '));
    }
    else {
      this.setState({ errors: {} });
    }

    const {
      choicedValue, choicedCity, choicedChannel, choicedStructure,
      choicedPrize, saleDate, number, countItems, CK
    } = this.state;

    const { name, fullname } = choicedChannel;
    const { city, weekday, weekdayFull, cityCode } = choicedCity;
    const { length } = choicedStructure;
    const day = saleDate ? new Date(saleDate) : new Date();
    const serial = Math.random().toString().replace(/..(.{6}).*/, '$1');
  
    const lottery = {
      number,
      value: choicedValue,
      length,
      type: 'dien-tu',
      day: day.toString(),
      prize: {
        prize_id: choicedPrize._id,
        code: choicedPrize.code,
        name: choicedPrize.name,
      },
      channel: {
        name,
        city,
        weekday,
        weekdayFull,
        cityCode,
        fullname,
        channel_id: choicedChannel._id,
      },
      count: countItems,
      discount: CK,
      serial,
    };

    this.showLoading();

    submitLottery(JSON.stringify(lottery))
      .then((res) => {
        alert('Cập Nhật Thành Công!!!');
        this.resetField()
        if(isPrint) {
          if(res) {
            this._printRecieve(res);
          }
        }
      })
      .catch(() => alert('Cập Nhật Thất Bại!!!'))
      .then(() => this.hideLoading());
  }

  fetchChannels() {
    const self = this;
    fetchMe().then(({_id, channels = []}) => {
      self.setState({ channels });
      if (channels.length) {
        Promise
        .all(channels.map(channel => fetchChannel(channel.channel_id)))
        .then((fetchedChannel = {})  => {
          console.log('fetchedChannel', fetchedChannel);
          self.setState({ channels: fetchedChannel }, () => {
            this.choiceChannel(this.state.channels[0].name);
          })
        })
        .catch(console.error);
      }
    });
  }

  fetchValueLotteries() {
    const self = this;
    fetchTypeLotteries().then((typeLotteries = []) => {
      console.log('typeLotteries', typeLotteries)
      self.setState({ valueLotteries: typeLotteries }, () => {
        self.choiceValueLottery(typeLotteries[0].value)
      });
    });
  }

  fetchPrizeStructures(_id) {
    const self = this;
    fetchPrizeStructures(_id).then((prizeStructures = []) => {
      console.log('prizeStructures', prizeStructures)
      self.setState({ prizeStructures }, () => {
        self.choiceStructure(prizeStructures[0].length)
      });
    });
  }

  renderTotal() {
    const { countItems, CK, choicedValue } = this.state;
    const value = choicedValue || '0';
    const price = parseInt(countItems || 0) * parseInt(value);
    const total = (1 - parseFloat(CK || 0)/100) * price;
    return (
      <View style={{ width, height: 60, flexDirection: 'row', borderBottomColor: 'black', borderBottomWidth: 1 }}> 
        <View style={{ width: (width * 40) / 100, height: 60, justifyContent: 'center', marginLeft: 10 }}>
          <Text style={{ color: 'black', fontSize: 30, fontWeight: 'bold' }}>Tổng Tiền: </Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginRight: 10 }}>
          <Text style={{ color: 'black', fontSize: 30, fontWeight: 'bold' }}>{formatCurrency(`${total || 0}`)}</Text>
        </View>
      </View>
    )
  }

  renderButton() {
    return (
      <View 
        style={{ 
          width, 
          height: 60,
          flexDirection: 'row',
          marginTop: 10,
        }}
      >
        <TouchableOpacity
          onPress={() => this.insert(false)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'red'
          }}
        >
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cập Nhật</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.insert(true)}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#00C853'
          }}
        >
        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Cập Nhật + In Vé</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderInput() {
    const { choicedChannel, saleDate, choicedCity, choicedValue, choicedStructure, choicedPrize, number, countItems, CK } = this.state;
    return (
      <View>
      <View style={{ width, height: 60, flexDirection: 'row', marginTop: 10 }}> 
        <TouchableOpacity 
          onPress={() => this.setState({ isShowModal: true })}
          style={{ width: (width - 15) / 2, height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Chọn Cty Xổ Số</Text>
          <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{Object.keys(choicedChannel).length ? choicedChannel.fullname : 'Chọn Cty Xổ Số'}</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          onPress={() => this.setState({ isDateTimePickerVisible: true })}
          style={{ width: (width - 15) / 2 , height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Chọn Ngày Xổ</Text>
          <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{`${saleDate.getDate()} - ${saleDate.getMonth() + 1} - ${saleDate.getFullYear()}`}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ width, height: 60, flexDirection: 'row', marginTop: 10 }}> 
        <View style={{ width: (width - 15) / 2, height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Đài Dự Thưởng</Text>
          <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{`${choicedCity.city}`}</Text>
        </View>
        <TouchableOpacity 
          onPress={() => this.setState({ isShowModalTypeNumber: true })}
          style={{ width: (width - 15) / 2 , height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Chọn Loại Số</Text>
          <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{choicedStructure.length}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ width, height: 60, flexDirection: 'row', marginTop: 10 }}> 
        <View style={{ width: (width - 15) / 2, height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Nhập Số Dự Thưởng</Text>
          <TextInput 
            {...commonInputProps}
            placeholder={'0'} 
            value={number}
            keyboardType={'numeric'}
            onChangeText={(text) => {
              this.changeNumber(text)
            }}
          />
        </View>
      <TouchableOpacity 
        onPress={() => this.setState({ isShowModalPrize : true })}
        style={{ width: (width - 15) / 2 , height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
        <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Chọn Lô</Text>
        <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{Object.keys(choicedPrize).length ? choicedPrize.name : 'Chọn Lô' }</Text>
      </TouchableOpacity>
      </View>
      <View style={{ width, height: 60, flexDirection: 'row', marginTop: 10 }}> 
        <TouchableOpacity 
          onPress={() => this.setState({ isShowModalValueLottery : true })}
          style={{ width: (width - 15) / 2, height: 60, marginLeft: 5, justifyContent: 'center', borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Mệnh Giá</Text>
          <Text style={{ color: 'black', fontSize: 18, fontWeight: 'bold', marginLeft: 5 }}>{formatCurrency(choicedValue)}</Text>
        </TouchableOpacity>
        <View style={{ width: (width - 15) / 2 , height: 60, marginLeft: 5, borderRadius: 5, borderColor: '#90A4AE', borderWidth: 1, flexDirection: 'row' }}>
          <View style={{ flex: 1, justifyContent: 'center', marginLeft: 5 }}>
            <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>Số Lượng</Text>
            <TextInput 
              {...commonInputProps}
              placeholder={'0'} 
              value={countItems}
              keyboardType={'numeric'}
              onChangeText={(text) => {
                this.setState({ countItems: text })
              }}
            />
          </View>
          <View style={{ height: 60, width: 1, backgroundColor: '#BDBDBD' }} />
          <View style={{ flex: 1, justifyContent: 'center', marginLeft: 5 }}>
          <Text style={{ color: '#BDBDBD', fontSize: 12, fontWeight: 'bold', marginLeft: 5 }}>CK(%)</Text>
            <TextInput 
              {...commonInputProps}
              placeholder={'0'} 
              value={CK}
              keyboardType={'numeric'}
              onChangeText={(text) => {
                this.setState({ CK: text })
              }}
            />
        </View>
        </View>
      </View>
    </View>
    ) 
  }

  renderHeader() {
    const { fetchListLotteries } = this.props.navigation.state.params;
    return (
      <View 
        style={{ 
          width, 
          height: 50,
          flexDirection: 'row'
        }}
      >
        <TouchableOpacity
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#0091EA'
          }}
          onPress={() => this.resetField()}
        >
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>Làm Mới</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            fetchListLotteries()
            this.props.navigation.goBack()
          }}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#78909C'
          }}
        >
        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>Xem DS Vé Bán</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderItemDropdown(item, index) {
    return (
      <TouchableOpacity 
        key={index}
        onPress={() => this.choiceChannel(item.name)}
        style={{ width, height: 100, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent:'center', alignItems: 'center' }}
      >
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}>{item.fullname || ''}</Text>
      </TouchableOpacity>
    );
  }

  renderItemDropdownTypeNumber(item, index) {
    return (
      <TouchableOpacity 
        key={index}
        onPress={() => this.choiceStructure(item.length)}
        style={{ width, height: 100, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent:'center', alignItems: 'center' }}
      >
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}>{item.length || ''}</Text>
      </TouchableOpacity>
    );
  }

  renderItemDropdownPrize(item, index) {
    return (
      <TouchableOpacity 
        key={index}
        onPress={() => this.choicePrize(item.code)}
        style={{ width, height: 100, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent:'center', alignItems: 'center' }}
      >
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}>{item.name || ''}</Text>
      </TouchableOpacity>
    );
  }

  renderItemDropdownValueLotteries(item, index) {
    return (
      <TouchableOpacity 
        key={index}
        onPress={() => this.choiceValueLottery(item.value)}
        style={{ width, height: 100, borderBottomColor: '#90A4AE', borderBottomWidth: 1, justifyContent:'center', alignItems: 'center' }}
      >
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', marginLeft: 5 }}>{formatCurrency(item.value) || ''}</Text>
      </TouchableOpacity>
    );
  }

  dismissModal(key) {
    switch(key) {
      case 'isShowModal': 
        this.setState({ isShowModal: false }) 
        break
      case 'isShowModalTypeNumber': 
        this.setState({ isShowModalTypeNumber: false }) 
        break
      case 'isShowModalPrize': 
        this.setState({ isShowModalPrize: false }) 
        break
      case 'isShowModalValueLottery': 
        this.setState({ isShowModalValueLottery: false }) 
        break
    }
  }

  renderHeaderModal(key, title) {
    return (
      <View style={{ height: 60, width, backgroundColor: '#F5F5F5', flexDirection: 'row' }} >
        <TouchableOpacity
          style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center' }}
          onPress={() => this.dismissModal(key)}
        >
          <Image source={require('../../images/close_red.png')}
            style={{ marginTop: 5, marginLeft: 5, width: 50, height: 50 }}/>
        </TouchableOpacity>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontSize: 20, color: '#4FC3F7', fontWeight: 'bold' }}>{title}</Text>
        </View>
        <View style={{ width: 60, height: 60 }} />
        <View style={{ backgroundColor: '#d3d3d3', height: 1, width, position: 'absolute', top: 79, left: 0 }}  />
      </View>
    )
  }

  renderModal() {
    const { channels } = this.state;
    return (
      <Modal
        style={{ width, height, backgroundColor: 'white' }}
        isOpen={this.state.isShowModal}
        swipeToClose={false}
        onClosed={() => this.setState({ isShowModal: false })}
      >
        {this.renderHeaderModal('isShowModal', 'Chọn Cty Phát Hành Vé')}
        <ScrollView>
          {channels.map((item, index) => this.renderItemDropdown(item, index))}
        </ScrollView>
      </Modal>
    )
  }

  renderModaltypeNumber() {
    const { prizeStructures } = this.state;
    return (
      <Modal
        style={{ width, height, backgroundColor: 'white' }}
        isOpen={this.state.isShowModalTypeNumber}
        swipeToClose={false}
        onClosed={() => this.setState({ isShowModalTypeNumber: false })}
      >
        {this.renderHeaderModal('isShowModalTypeNumber', 'Chọn Loại Số')}
        <ScrollView>
          {prizeStructures.map((item, index) => this.renderItemDropdownTypeNumber(item, index))}
        </ScrollView>
      </Modal>
    )
  }

  renderModalPrizes() {
    const { prizes } = this.state;
    
    return (
      <Modal
        style={{ width, height, backgroundColor: 'white' }}
        isOpen={this.state.isShowModalPrize}
        swipeToClose={false}
        onClosed={() => this.setState({ isShowModalPrize: false })}
      >
        {this.renderHeaderModal('isShowModalPrize', 'Chọn Loại Lô')}
        <ScrollView>
          {prizes.map((item, index) => this.renderItemDropdownPrize(item, index))}
        </ScrollView>
      </Modal>
    )
  }

  renderModalValueLotteries() {
    const { valueLotteries } = this.state;
    return (
      <Modal
        style={{ width, height, backgroundColor: 'white' }}
        isOpen={this.state.isShowModalValueLottery}
        swipeToClose={false}
        onClosed={() => this.setState({ isShowModalValueLottery: false })}
      >
        {this.renderHeaderModal('isShowModalValueLottery', 'Chọn Loại Mệnh Giá')}
        <ScrollView>
          {valueLotteries.map((item, index) => this.renderItemDropdownValueLotteries(item, index))}
        </ScrollView>
      </Modal>
    )
  }

  choiceChannel(v) {
    const { channels, errors } = this.state;

    const choicedChannel = channels.filter(c => c.name === v)[0] || {};
    const cities = choicedChannel.weekdays || [];
    this.fetchPrizeStructures(choicedChannel._id)
    errors.channel = '';
    this.setCity(cities, this.state.saleDate);
    this.setState({ choicedChannel, cities, errors, isShowModal: false });
  }

  choiceDate(date) {
    this.setCity(this.state.cities, date);
    this.setState({ saleDate: date });
  }

  setCity(cities, date) {
    const weekday = date.getDay() === 0 ? 'chunhat' : `thu${date.getDay() + 1}`;
    const choicedCity = cities.filter(city => city.weekday === weekday)[0] || {};
    this.setState({ choicedCity });
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  
  _handleDatePicked = (date) => {
    this.choiceDate(date)
    this._hideDateTimePicker();
  };
  

  renderDate() {
    return (
      <DateTimePicker
        isVisible={this.state.isDateTimePickerVisible}
        onConfirm={this._handleDatePicked}
        onCancel={this._hideDateTimePicker}
      />
    )
  }

  parseNumber(number) {
    return (''+number).split("").map(v => readNumber[v]).join(" ")
  }

  async _printRecieve(item){
        const {
          _id, number, channel, prize, value, count, day
        } = item;
        let parse = this.parseNumber(number);
        let daySale = day.split('-');
      try {
        await SunmiInnerPrinter.setAlignment(1);
        await SunmiInnerPrinter.setFontSize(36);
        await SunmiInnerPrinter.printOriginalText("CTY TNHH C409\n");
        await SunmiInnerPrinter.setAlignment(1);
        await SunmiInnerPrinter.setFontSize(36);
        await SunmiInnerPrinter.printOriginalText("BIÊN NHẬN MUA HỘ VÉ SỐ TỰ CHỌN\n");

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(20);
        await SunmiInnerPrinter.printOriginalText("*************************************\n");
        

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Code: ${_id}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Đài Dự Thưởng: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${channel.city}\n`);

        
        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Ngày Dự Thưởng: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${daySale[2]} - ${daySale[1]} - ${daySale[0]}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Lô Dự Thưởng: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${prize.name}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Số DT(Số): `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${number}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Số DT(Chữ): `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${parse}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Mệnh Giá: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${formatCurrency(value)}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(24);
        await SunmiInnerPrinter.printOriginalText(`Số lượng: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${count}\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(28);
        await SunmiInnerPrinter.printOriginalText(`Thành Tiền: `);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(30);
        await SunmiInnerPrinter.printOriginalText(`${formatCurrency(count * parseInt(value))}đ\n`);

        await SunmiInnerPrinter.setAlignment(0);
        await SunmiInnerPrinter.setFontSize(20);
        await SunmiInnerPrinter.printOriginalText("*************************************\n");

        await SunmiInnerPrinter.setAlignment(1);
        await SunmiInnerPrinter.setFontSize(22);
        await SunmiInnerPrinter.printOriginalText('BIÊN NHẬN CÓ GIÁ TRỊ KHI QUÝ KHÁCH NHẬN ĐƯỢC HÌNH ẢNH SCAN VÉ XSKT\n\n');

        await SunmiInnerPrinter.setAlignment(1);
        await SunmiInnerPrinter.printQRCode(_id, 5, 1);
        await SunmiInnerPrinter.printOriginalText("\n\n\n\n");
      }catch(e){
        console.log(e)
        alert("print error."+e.message);
      }
  }

  render() {
    const { visible, isReady } = this.state;
    if(!isReady) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Dữ Liệu Chưa Sẵn Sàng</Text>
          <TouchableOpacity
            onPress={() => this.callAPI()}
          >
            <Text>Thử Lại</Text>
          </TouchableOpacity>
        </View>
      )
    }
    return (
      <KeyboardAvoidingView behavior='height' style={styles.container}>
        <ScrollView>
          {this.renderHeader()}
          {this.renderTotal()}
          {this.renderInput()}
          {this.renderButton()}
        </ScrollView>
        {this.renderModal()}
        {this.renderModaltypeNumber()}
        {this.renderDate()}
        {this.renderModalPrizes()}
        {this.renderModalValueLotteries()}
        <Spinner visible={visible} textStyle={{ color: '#FFF' }}/>
      </KeyboardAvoidingView>
    );
  }
}

const commonInputProps = {
  height: 25,
  flex: 1,
  fontSize: 18,
  fontWeight: 'bold',
  underlineColorAndroid: 'transparent',
  placeholderTextColor: 'black',
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width: 24,
    height: 24
  }
});

export default connect(state => ({user: state.authentication.user}))(SaleTickets);