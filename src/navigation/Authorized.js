import React from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import ListTicket from '../components/ticket';
import CheckTicket from '../components/ticket/checkTicket';
import SaleTicket from '../components/ticket/saleTicket';
import DrawerContent from '../components/drawer';

const Authorized = StackNavigator({
    ListTicket: { screen: ListTicket },
    SaleTicket: { screen: SaleTicket},
    CheckTicket: { screen: CheckTicket },
})
export default Authorized;