const API_URL = 'http://128.199.151.182/staging'

const STATUS_ERROR = {
  WRONG_PASSWORD: 'Mật khẩu hoặc Tên Đăng Nhập không đúng',
  UNREGISTERED_USER: 'Mật khẩu hoặc Tên Đăng Nhập không đúng',
  'Permission Denied': 'Hết hạn đăng nhập'
};

function parseJSON(response) {
  return new Promise((resolve) => response.json()
    .then((json) => resolve({
      status: response.status,
      ok: response.ok,
      json,
    })));
}

function fetchSomething(apiPath, method, body) {
  return new Promise((resolve, reject) => {
    fetch(apiPath, {
      method,
      credentials: 'include',
      body,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(parseJSON)
      .then((response) => {
        if (response.ok) {
          return resolve(response.json);
        }
        return reject(STATUS_ERROR[response.json.error] ? STATUS_ERROR[response.json.error] : 'Lỗi Không Xác Định');
      })
      .catch((error) => reject('Lỗi không kết nối mạng'));
  });
}

function login(data) {
  return fetchSomething(`${API_URL}/auth/local`, 'post', data);
}

function fetchMe() {
  return fetchSomething(`${API_URL}/me`, 'get');
}

function fetchChannel(id) {
  return fetchSomething(`${API_URL}/channel/${id}`, 'get');
}

function fetchTypeLotteries() {
  return fetchSomething(`${API_URL}/typeLotteries`, 'get');
}

function fetchPrizeStructures(channel_id) {
  return fetchSomething(`${API_URL}/prizeStructures?channel_id=${channel_id}`, 'get');
}

function insertUser(body) {
  return fetchSomething(`${API_URL}/signup`, 'post', body);
}

function fetchListUsers() {
  return fetchSomething(`${API_URL}/users`, 'get');
}

function fetchUserById(id) {
  return fetchSomething(`${API_URL}/user/${id}`, 'get');
}

function submitLottery(params) {
  return fetchSomething(`${API_URL}/lottery`, 'post', params);
}

function fetchStatistics(id, params) {
  return fetchSomething(`${API_URL}/lottery/statistics/${id}`, 'get');
}

function fetchLottery(params) {
  console.log('url', `${API_URL}/lotteries?${params}`)
  return fetchSomething(`${API_URL}/lotteries?${params}`, 'get');
}

function fetchLotteryById(_id) {
  return fetchSomething(`${API_URL}/lottery/${_id}`, 'get');
}

function formatCurrency(number) {
  return (''+number).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}

function formatPercent(number) {
  return number + '%';
}

export {
  API_URL,
  login,
  fetchMe,
  fetchChannel,
  fetchTypeLotteries,
  fetchPrizeStructures,
  fetchListUsers,
  insertUser,
  fetchUserById,
  submitLottery,
  fetchStatistics,
  fetchLottery,
  formatCurrency,
  formatPercent,
  fetchLotteryById,
};
